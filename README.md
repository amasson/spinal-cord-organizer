# Spinal Cord Organizer

## Installation

- Optionnally create a virtual environment `python -m venv scoenv` and activate it `source scoenv/bin/activate`.
- Install requirements using `pip install -r requirements.txt`

## Usage

Scripts to organize spinal cord images.

Once you downloaded the datasets with [shanoir_downloader](https://github.com/Inria-Empenn/shanoir_downloader), your data will be organized in this way:

```
/path/to/downloaded/datasets/
├── downloaded_datasets.tsv
├── downloads.log
├── missing_datasets.tsv
├── raw 					# contains all niftis
│   ├── 100279
│   │   └── 20_t2_tse_sag__2_paliers_.nii.gz
│   ├── 100280
│   │   └── 21_t2_tse_sag.nii.gz
│   ├── 100285
│   │   └── 22_t2_medic_ax.nii.gz
│   └── ...
```

This folder will be named `datasets_path` thereafter.

This script structures those datasets in the following manner:

`patient_name / acquisition_date / part_number / f'{modality}{orientation}__{file_name}'`

where `part_number` is:
 - -1 for comp images,
 - 0 for brain images,
 - 1 for cerv images,
 - 2 for thor images,
 - etc. 

`modality` is `t2s`, `t2`, `stir` or `ofsepDualTse`.
`orientation` is `Ax` or `Sag` and `file_name` is given from `sequence_name` or `seq_nm_norm1`.

For example:
```
/data/amasson/medullar/datasets3/structured/
├── 1
│   ├── 2016-02-24
│   │   ├── 1
│   │   │   ├── stirSag__27_t2_stir_tse_sag_te_court.nii.gz
│   │   │   ├── t2Sag__26_t2_tse_sag_1er_palier.nii.gz
│   │   │   └── t2sAx__30_t2_medic_ax.nii.gz
│   │   └── 2
│   │       └── stirSag__28_t2_stir_tse_sag_te_court.nii.gz
│   └── 2019-04-25
│       ├── 1
│       │   ├── stirSag__16_stir_sag.nii.gz
│       │   └── t2Sag__19_t2_tse_sag__2_paliers_.nii.gz
│       └── 2
│           ├── stirSag__17_stir_sag.nii.gz
│           └── t2Sag__20_t2_tse_sag__2_paliers_.nii.gz
├── 10
│  ...
```

Formerly the structure was different, see the section "Previous structure" at the end.

For that you will use the `structure_datasets.py` script with the excel file describing your datasets and the path to the raw datasets. 

Organizing datasets involves:
- reorienting images (since we want to sort them by origin.y) ; images will be saved in the reoriented folder in the `datasets_path`
- creating previews
- filtering unwanted images like DTI, GADO and comp images
- sorting images by origin on the Y axis (note that the Y axis in anima sagittal orientation is lower at the head and greater at the feet)
- classify the images with the classes ['brain', 'axial', 'cervical', 'thoracic'] with a resnet18 model fine tuned for that purposed with fastai
- save the sorted images data in an excel file
- manually edit this excel file (relabel images) with the dedicated web interface. Comp should be labeled as -1, brain as 0, cerv as 1, thor as 2, etc.
- use the excel file to create a structured folder: patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{original_file_name}'

This happens in three steps:
1. Sort datasets with `python structure_datasets.py --sort`: it will create an excel file (data_sheet_sorted.xlsx) with the sorted datasets.
2. Edit this excel file to relabel images with the dedicated web interface if necessary.
3. Organize datasets with the `python structure_datasets.py --organize`. This will create a structured folder: patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{original_file_name}'

### 1. Sort datasets

Use `python structure_datasets.py --datasets_path /path/to/your/datasets/ --data_sheet /path/to/data_sheet.xlsx --sort` to sort the datasets and generate a new excel file editable with the web interface.

This will create a `reoriented` folder (a copy of the `raw` folder, but with `SAGITTAL` oriented images and image previews) in the `datasets_path`, and a `/path/to/data_sheet_sorted.xlsx` file which will be editable with the web interface.

### 2. Edit the sorted excel file with the web interface

Create a symlink of your the reoriented folder in this directory (name it `previews` for example), with the following command:
`ln -s /path/to/your/datasets/reoriented /path/to/spinal-cord-organizer/previews`

Modify `dataset_list_url` and `preview_path` at the beginning of `table.js` so that it loads your file automatically.

Use `python -m http.server 8080` to start a server for the web interface, and navigate to `http://localhost:8080/`.

This will open a web page which will display the data sheet with preview images.

Datasets are ordered by patients > date > sequence_type by default.

You can filter or sort by sequence type (`seq_nm_norm1`), sequence name, originY_rank, etc.
For example, you can view all sagittal T1 images (filter `seq_nm_norm1` with `SAG T1`). Then, you can add another filter to view only those where `originY_rank` equals 1. 

You can also reorder columns, which is really handy to display important columns first (patient name, acquisition date, sequence type, originY_rank, image).

Edit the `originY_rank` column if there are errors ; labels should be:
 - -1 for comp images,
 - 0 for brain images,
 - 1 for cerv images,
 - 2 for thor images,
 - etc. 

The automatic classfier should have done a good job, and difference between the model prediction and the inference from meta-data is highlighted in the `review` column.

It is handy to process all `SAG T1` images, then all `SAG STIR` images, than all `SAG T2` images ; but other ways could be handy as well.

Once you fixed all dataset labels, click `save` to save the final excel file.

### 3. Organize datasets

Once all datasets are correctly labelled, use `python structure_datasets.py --datasets_path /path/to/your/datasets/ --data_sheet /path/to/data_sheet.xlsx --organize` to organize the datasets in a structured folder named `patient_date_part`: 
patient_date_part / patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{original_file_name}'

### Previous structure

Formerly the structure was `patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{original_file_name}'`

For example:

```
/path/to/downloaded/datasets/patient_date_part
├── patient01
│   ├── 2019-06-14
│   │   ├── 0 											# brain images
│   │   │   ├── AX_T2_ax1_6_ax_t2_fse.nii.gz 			# f'{sequence_name}{axial_suffix}_{original_file_name}'
│   │   │   └── SAG_T1_401_sag_t1.nii.gz
│   │   └── 1  											# cerv images
│   │       ├── AX_T2_STAR_ax1_204183_204183-2D_MERGE_1-2D_MERGE_dcm_2D_MERGE_20190614000000_12.nii.gz
│   │       ├── AX_T2_STAR_ax2_12_2d_merge.nii.gz
│   │       ├── SAG_STIR_13_rcd_sag_t2_stir.nii.gz
│   │       └── SAG_T2_10_rcd_sag_t2.nii.gz
│   └── 2020-06-12
│       ├── 0 											# brain images
│       │   ├── AX_T2_ax1_6_ax_t2_fse.nii.gz
│       │   └── SAG_T1_204415_204415-SAG_T1-SAG_T1_dcm_SAG_T1_20200612000000_401.nii.gz
│       └── 1 											# cerv images
│           ├── AX_T2_STAR_ax1_14_2d_merge.nii.gz
│           ├── SAG_STIR_13_rcd_sag_t2_stir.nii.gz
│           └── SAG_T2_11_rcd_sag_t2.nii.gz
├── patient02
│   ├── 2014-09-17
│   ...
```