// const dataset_list_url = "STIR-T2_SEQ_sorted.xlsx"
// const dataset_list_url = "datasets3_sorted_with_ax.xlsx"
// const dataset_list_url = "datasets3_sorted_finished.xlsx"
// const dataset_list_url = "datasets3_latest.xlsx"
// const dataset_list_url = "datasheets/datasets3_completed.xlsx"
const dataset_list_url = 'datasheets/datasets_to_download_4_sorted.xlsx'

class ImageRenderer {
	// gets called once before the renderer is used
	init(params) {
		// create the cell
		this.eGui = document.createElement('div');
		// replace .nii.gz by _preview_0.png in the params.value string
		if (params.value == null ){
			return
		}
		let preview_path = params.value.replace('.nii.gz', '_preview_0.png');
		// remove /data/amasson/medullar/datasets2/ from preview_path
		// preview_path = preview_path.replace('/data/amasson/medullar/datasets2/reoriented', 'dataset2_reoriented');
		preview_path = 'reoriented/' + preview_path
		this.eGui.innerHTML = `<img class="table" src="${preview_path}" max-height="200px" >`;
	}

	getGui() {
		return this.eGui;
	}

	refresh(params) {
		return true;
	}

	destroy() {
	}
}


let workbook = null;
let sheet_name = null;
let XL_row_object = null;

let save_in_local_storage = function(data) {
	localStorage.setItem('table_data', JSON.stringify(data));
}

let load_from_local_storage = function() {
	let data = localStorage.getItem('table_data');
	if (data) {
		return JSON.parse(data);
	}
	return null;
}

let download_excel = function() {
	workbook.Sheets[sheet_name] = XLSX.utils.json_to_sheet(XL_row_object);
	XLSX.writeFile(workbook, dataset_list_url);
}

document.addEventListener('DOMContentLoaded', function (event) {
	// Load the datasets file from URL dataset_list_url

	document.getElementById('save').addEventListener('click', function() {
		download_excel();
	});

	(async () => {
		const url = dataset_list_url
		const data = await (await fetch(url)).arrayBuffer();
		
		// Load the file with acquisition_date as a date column
		workbook = XLSX.read(data);

		sheet_name = workbook.SheetNames[0];
		// Get the data from the first sheet as a JS object, with the following header types { originY: number, endY: number, axial_rank: number, acquisition_date: date }
		XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name]);
		for (let i = 0; i < XL_row_object.length; i++) {
			XL_row_object[i]['acquisition_date'] = new Date(XL_row_object[i]['acquisition_date']);
			XL_row_object[i]['originY'] = parseFloat(XL_row_object[i]['originY']);
			XL_row_object[i]['endY'] = parseFloat(XL_row_object[i]['endY']);
		}
		// XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheet_name], {raw: false});
		// XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name], {raw: false});


		// Read the header from the first item
		var header = Object.keys(XL_row_object[0]);
		// Create column defs from the header
		var columnDefs = header.map(function (key) {
			let def = {
				headerName: key,
				field: key, 
				sortable: true, 
				filter: true,
				resizable: true,
				width: 125,
			};
			if (key == 'image_path' || key == 'image_preview_path') {
				def.cellRenderer = 'imageRenderer';
				def.width = 200;
			}
			if (key == 'originY_rank') {
				def.editable = true;
			}
			if (key == 'comment') {
				def.editable = true;
			}
			return def;
		});
		// Create the grid options, make the cell height to 200px
		var gridOptions = {
			columnDefs: columnDefs,
			components: { imageRenderer: ImageRenderer },
			rowData: XL_row_object,
			rowHeight: 200,
			// the change event handler to update the data when originY_rank changes
			onCellValueChanged: (event)=> {
				console.log(event.colDef.field)
				console.log(event.data)
				console.log(event.data.originY_rank)
				save_in_local_storage(XL_row_object)
			}
		};
		// Create the grid
		let gridDiv = document.querySelector('#grid')
		new agGrid.Grid(gridDiv, gridOptions)

	})()
});