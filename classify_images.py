
# %% 

import shutil
from pathlib import Path
import pandas
from functools import partial

# from fastai.torch_core import TensorBase
# from fastcore.all import 
from fastai.vision.learner import vision_learner
from fastai.vision.all import PILImage, Interpretation, resize_images, verify_images, DataBlock, ImageBlock, ImageDataLoaders, CategoryBlock, MultiCategoryBlock, get_image_files, RandomSplitter, Resize, parent_label, cnn_learner, resnet18, F1ScoreMulti, accuracy_multi, error_rate, load_learner
import PIL
from PIL import Image

# %% 

# learn_inf = load_learner('models/classifier.pkl')

# pp = Path('/data/amasson/medullar/datasets4/reoriented/')

# dfc = pandas.read_excel('../datasheets/datasets_to_download_4_sorted_completed.xlsx')

# dfc = dfc.drop_duplicates('sequence_id')

# test_files = [pp / image for image in dfc.image_preview_path]

# test_dl = learn_inf.dls.test_dl(test_files)

# preds = learn_inf.get_preds(dl=test_dl, with_decoded=True)

# dfc['preds_original'] = learn_inf.dls.vocab[preds[2]]

# test_path = Path('/data/amasson/medullar/datasets4/classification/test')
# test_path.mkdir(exist_ok=True, parents=True)

# for image_path in test_files:

# 	im = Image.open(image_path)
# 	out = im.transpose(PIL.Image.FLIP_TOP_BOTTOM)

# 	out.save(test_path / f'{image_path.parent.name}_{image_path.name}')

# resize_images(test_path, max_size=400, dest=test_path)
#%%


test_files = [test_path / image.replace('/', '_') for image in dfc.image_preview_path]

test_dl = learn_inf.dls.test_dl(test_files)

preds = learn_inf.get_preds(dl=test_dl, with_decoded=True)


dfc['preds_resized'] = learn_inf.dls.vocab[preds[2]]

(dfc.preds_original != dfc.preds_resized).sum()
# %% 

# structured = Path('/data/amasson/medullar/datasets3/structured')
reoriented = Path('/data/amasson/medullar/datasets3/reoriented')
classification_path = Path('/data/amasson/medullar/datasets3/classification')

classes = ['axial', 'brain', 'cervical', 'thoracic'] #, 'comp']
# %% 

datasets = pandas.read_csv('/data/amasson/medullar/datasets3/datasets3_completed.csv')

for c in classes:
	folder = classification_path / c
	folder.mkdir(exist_ok=True, parents=True)


for index, row in datasets.iterrows():
	print(row['sequence_id'])
	if row['axial'] and row['originY_rank'] != 0:
		(classification_path / 'axial' / f'{row["sequence_id"].png}').symlink_to(reoriented / row['image_preview_path'])
		continue
	if row['originY_rank'] == 0:
		(classification_path / 'brain' / f'{row["sequence_id"].png}').symlink_to(reoriented / row['image_preview_path'])
	elif row['originY_rank'] == 1:
		(classification_path / 'cervical' / f'{row["sequence_id"].png}').symlink_to(reoriented / row['image_preview_path'])
	elif row['originY_rank'] >= 2:
		(classification_path / 'thoracic' / f'{row["sequence_id"].png}').symlink_to(reoriented / row['image_preview_path'])
	# elif row['originY_rank'] == -1:
	# 	(classification_path / 'comp' / f'{row["sequence_id"].png}').symlink_to(reoriented / row['image_preview_path'])
	else:
		print('    not classified')

for c in classes:
	folder = classification_path / c
	resize_images(folder, max_size=400, dest=folder)
	failed = verify_images(folder)
	failed.map(Path.unlink)
	print(len(failed))


# %% 
dls = DataBlock(
	blocks=(ImageBlock, CategoryBlock), 
	get_items=get_image_files, 
	splitter=RandomSplitter(valid_pct=0.2, seed=42),
	get_y=parent_label,
	item_tfms=[Resize(192, method='squish')]
).dataloaders(classification_path, bs=32)

# %% 
dls.show_batch(max_n=6)
#from matplotlib import pyplot
#pyplot.show()

# %% 
# dls.show_batch(max_n=6)


# f1_macro = F1ScoreMulti(thresh=0.5, average='samples')
# #f1_macro.name = 'F1(macro)'
# f1_samples = F1ScoreMulti(thresh=0.5, average='samples')
# #f1_samples.name = 'F1(samples)'

# learn = vision_learner(dls, resnet18, metrics=[partial(accuracy_multi, thresh=0.5), f1_macro, f1_samples])
# %%
learn = vision_learner(dls, resnet18, metrics=error_rate)
# %%
learn.lr_find()

# print(learn.lr_find())
# %%
learn.fine_tune(10,3e-3)
# %%
learn.show_results(max_n=100)

# %%

interp = Interpretation.from_learner(learn)

interp.plot_top_losses(9, figsize=(15,10))

# %%
learn.save(classification_path.parent/'classifier')
# %%

learn.load(classification_path.parent/'classifier')
# %% 


# structured = Path('/data/amasson/medullar/datasets3/structured')
reoriented = Path('/data/amasson/medullar/datasets2/reoriented')
test_classification_path = Path('/data/amasson/medullar/datasets2/classification')

classes = ['axial', 'brain', 'cervical', 'thoracic'] #, 'comp']

# %%

test_datasets = pandas.read_excel('/data/amasson/medullar/datasets2/STIR-T2_SEQ_COMPLETED_4.xlsx')

for c in classes:
	folder = test_classification_path / c
	folder.mkdir(exist_ok=True, parents=True)

def copyfile(source, destination):
	source = Path(source)
	if source.exists():
		shutil.copyfile(source, destination)
	return

for index, row in test_datasets.iterrows():
	print(row['sequence_id'])
	preview = row['image_path'].replace('.nii.gz', '_preview_0.png')
	if row['axial_rank'] > 0 and row['origin2_rank'] != 0:
		copyfile(preview, test_classification_path / 'axial' / f'{row["sequence_id"]}.png')
		continue
	if row['origin2_rank'] == 0:
		copyfile(preview, test_classification_path / 'brain' / f'{row["sequence_id"]}.png')
	elif row['origin2_rank'] == 1:
		copyfile(preview, test_classification_path / 'cervical' / f'{row["sequence_id"]}.png')
	elif row['origin2_rank'] >= 2:
		copyfile(preview, test_classification_path / 'thoracic' / f'{row["sequence_id"]}.png')
	# elif row['origin2_rank'] == -1:
	# 	copyfile(preview, test_classification_path / 'comp' / f'{row["sequence_id"]}.png')
	else:
		print('    not classified')

for c in classes:
	folder = test_classification_path / c
	resize_images(folder, max_size=400, dest=folder)
	failed = verify_images(folder)
	failed.map(Path.unlink)
	print(len(failed))



# %% 
# import PIL
# from PIL import Image


# for c in classes:
# 	folder = test_classification_path / c
# 	for file in folder.iterdir():
# 		image = Image.open(str(file))
# 		image = image.transpose(PIL.Image.FLIP_TOP_BOTTOM)
# 		image.save(file)

# %%
# Show test batch:

valid_dls = DataBlock(
	blocks=(ImageBlock, CategoryBlock), 
	get_items=get_image_files, 
	splitter=RandomSplitter(valid_pct=0.2, seed=42),
	get_y=parent_label,
	item_tfms=[Resize(192, method='squish')]
).dataloaders(test_classification_path, bs=32)

# %% 
valid_dls.show_batch(max_n=6)



# %%

label,_,probs = learn.predict(PILImage.create('/data/amasson/medullar/datasets2/reoriented/14363/7_sag_t2_tse_dorso_lomb_preview_0.png'))
print(label)
print(_)
print(probs)

# %%

tst_files = get_image_files(test_classification_path)

tst_dl = dls.test_dl(tst_files)

tst_dl.show_batch(max_n=20)

# %%

learn.validate(dl=tst_dl)
# %%

learn.show_results(dl=tst_dl)
# %%

preds = learn.get_preds(dl=tst_dl, with_decoded=True)

# %%
min_prob = preds[0].max(axis=1).values.min()
#%%

min_prob.item()
# %%
import pandas

results = [{'prediction': classes[preds[2][i].item()], 'label': file.parent.name, 'file': file, 'probability': preds[0].max(axis=1).values[i].item()} for i, file in enumerate(tst_files)]
df = pandas.DataFrame.from_records(results)
df.to_csv(test_classification_path.parent/'results.csv', index=False)


# %%


from PIL import Image
from io import BytesIO
from IPython.display import HTML
import base64

pandas.set_option('display.max_colwidth', -1)

# def get_thumbnail(path):
#     path = "\\\\?\\"+path # This "\\\\?\\" is used to prevent problems with long Windows paths
#     i = Image.open(path)    
#     return i

def image_base64(path):
    im = Image.open(path)
    with BytesIO() as buffer:
        im.save(buffer, 'jpeg')
        return base64.b64encode(buffer.getvalue()).decode()

def image_formatter(im):
    return f'<img src="data:image/jpeg;base64,{image_base64(im)}">'

# %%
import pandas
df = pandas.read_csv('/data/amasson/medullar/datasets2/results.csv')
#df = df[df.label != 'comp']
len(df), len(df[df.prediction != df.label])

# %%

df[df.prediction != df.label]

# %%

HTML(df[df.prediction != df.label].to_html(formatters={'file': image_formatter}, escape=False))


#%%

learn.export('/data/amasson/medullar/classifier.pkl')

#%%
!ls -alht '/data/amasson/medullar/classifier.pkl'
#%% 
!ls -alh '/data/amasson/medullar/datasets3/classifier.pth'

#%%

learn_inf = load_learner('/data/amasson/medullar/classifier.pkl')

#%%

# !ls /data/amasson/medullar/datasets2/classification/cervical/172322.png

learn_inf.predict('/data/amasson/medullar/datasets2/classification/cervical/172322.png')
#%%
df.loc[(df.prediction == 'brain') & (df.label == 'brain'), 'probability'] -= 1
#%%
df.groupby(['label']).probability.transform(lambda x: x.mean())
