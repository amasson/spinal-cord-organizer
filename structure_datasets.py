import datetime
from fileinput import filename
import re
import textwrap
import argparse
from curses import raw
import shutil
from anima_utils import *
import pandas
import SimpleITK as sitk
import numpy as np
from pathlib import Path
import PIL
import tempfile
from PIL import Image
from fastai.vision.all import load_learner, resize_images

# ----------------------- #
# --- Reorient images --- #
# ----------------------- #

def reorient_image(image_path, destination_path):
	call([animaConvertImage, '-i', image_path, '-R', 'SAGITTAL', '-o', destination_path])

# Reorient images
def reorient_images(raw_path, reoriented_path):
	# - for all niftis in the raw_path, reorient image and write it to the reoriented directory
	niftis = list(raw_path.glob('**/*.nii.gz'))
	for nifti in niftis:
		print(nifti)
		destination_path = reoriented_path / nifti.relative_to(raw_path)
		destination_path.parent.mkdir(parents=True, exist_ok=True)
		if not destination_path.exists():
			reorient_image(str(nifti), str(destination_path))

# ------------------------------#
# --- Create image previews --- #
# ------------------------------#

def save_image(image, path, flipX=False, flipY=False):
    pillow_image = Image.fromarray( image.astype(np.uint8) )
    if flipX:
        pillow_image = pillow_image.transpose(Image.FLIP_LEFT_RIGHT)
    if flipY:
        pillow_image = pillow_image.transpose(Image.FLIP_TOP_BOTTOM)
    print('save image ', path, ' of size ', pillow_image.size)
    pillow_image.save(path, format='png')
    return

def save_slices(image_to_display, center, path, sizes=None):
    image1 = image_to_display[center[0], :, :]
    image2 = image_to_display[:, center[1], :]
    image3 = image_to_display[:, :, center[2]]
    flips = [(False, False), (False, False), (False, False)]
    for i, image in enumerate([image1, image2, image3]):
        save_image(image, f'{str(path).replace(".png", "")}_{i}.png', flips[i][0], flips[i][1])
    return

def save_center_slice(image_data, path, sizes=None):
	size = image_data.shape
	center = [size[0] // 2, size[1] // 2, size[2] // 2]
	save_slices(image_data, center, path, sizes)
	return

# put image_data in the range [0, 255] 
def normalize_image(image):
	image_data = sitk.GetArrayFromImage(image)
	image_min, image_max = image_data.min(), image_data.max()
	if image_max > image_min:
		image_data = (image_data - image_min) / (image_max - image_min)
		image_data = image_data * 255
	else:
		image_data = np.zeros(image_data.shape)
	return image_data

def resample(image, new_spacing=[0.5, 0.5, 0.5]):
    size = image.GetSize()
    spacing = image.GetSpacing()
    resampler = sitk.ResampleImageFilter()
    resampler.SetOutputSpacing(new_spacing)
    new_size = [int(round(size[i] * spacing[i] / new_spacing[i])) for i in range(3)]
    resampler.SetSize(new_size)
    resampler.SetOutputDirection(image.GetDirection())
    resampler.SetOutputOrigin(image.GetOrigin())
    resampler.SetInterpolator(sitk.sitkLinear)
    return resampler.Execute(image)

# Create image previews for each dataset
def create_image_previews(reoriented_path, dataset_list):
	for index, row in dataset_list.iterrows():
		sequence_id = str(row['sequence_id'])
		# get the first nifti in reoriented_path / sequence_id
		sequence_folder = reoriented_path / sequence_id
		image_paths = list(sequence_folder.glob('**/*.nii.gz'))
		# warn if no nifti is found or more than one nifti is found
		if len(image_paths) == 0:
			print('no nifti found for sequence ', sequence_id)
		elif len(image_paths) > 1:
			print('more than one nifti found for sequence ', sequence_id)
		for image_path in image_paths:
			# set row['image_preview'] to the first image preview
			dataset_list.loc[index, 'image_path'] = str(image_path.relative_to(reoriented_path))
			dataset_list.loc[index, 'image_preview_path'] = str(image_path.relative_to(reoriented_path)).replace('.nii.gz', '_preview_0.png')
			if len(list(image_path.parent.glob('*_preview_*.png'))) > 0: continue
			image = sitk.ReadImage(str(image_path))
			if len(image.GetSize()) > 3:
				print('warning: image ', image_path, ' has more than 3 dimensions')
				continue
			image = resample(image)
			image_data = normalize_image(image)
			save_center_slice(image_data, str(image_path).replace('.nii.gz', '_preview.png'))

# ---------------------------------#
# --- Filter and sort datasets --- #
# ---------------------------------#

def filter_datasets(dataset_list):

	# remove the datasets for which seq_nm_norm1 contain GADO or DTI
	dataset_list = dataset_list[~dataset_list['seq_nm_norm1'].str.contains('GADO')]
	dataset_list = dataset_list[~dataset_list['seq_nm_norm1'].str.contains('DTI')]
	return dataset_list

# print('Sort datasets by origin on axis Y...')

# Add the origin and end of the axis Y of the dataset image in the dataset list
def add_origin_end(reoriented_path, dataset_list, reviewed_dataset_list):
	# For each dataset:
	# - get the sequence_id from the sequence_id column
	# - get the patient name from the id_a column
	# - get the sequence name from the seq_nm_norm1 column
	# - get the acquisition_date from the acquisition_date column
	# - find the niftis *.nii.gz from the path patients_path / patient_name / acquisition_date / sequence_name / sequence_id
	# - assert there is only one nifti
	# - resolve nifti path and add it to the image_path column
	# - read the first nifti, and get the origin, size and spacing of the axis Y
	# - add the nifti origin to the origin column
	# - add the nifti end (origin + size * spacing) to the end column
	axis = 2
	for index, row in dataset_list.iterrows():
		sequence_id = str(row['sequence_id'])
		if reviewed_dataset_list is not None:
			matching_index = reviewed_dataset_list[reviewed_dataset_list.sequence_id == sequence_id].first_valid_index()
			if matching_index:
				for key in ['originY', 'endY', 'axial']:
					dataset_list.loc[index, key] = reviewed_dataset_list.at[matching_index, key]
				continue
		patient_name = str(row['id_a'])
		sequence_name = row['seq_nm_norm1']
		raw_acquisition_date = row['acquisition_date']
		# format acquisition_date to year-month-day
		acquisition_date = raw_acquisition_date if type(raw_acquisition_date) == str else raw_acquisition_date.strftime('%Y-%m-%d')
		if pandas.isna(row['image_path']): continue
		image_path = reoriented_path / row['image_path']
		image = sitk.ReadImage(str(image_path))
		origin = image.GetOrigin()
		size = image.GetSize()
		end = image.TransformIndexToPhysicalPoint(size)
		dataset_list.at[index, 'originY'] = origin[axis]
		dataset_list.at[index, 'endY'] = end[axis]
		dataset_list.at[index, 'axial'] = size[1] < size[0]/1.5 and size[1] < size[2]/1.5 or 'ax' in sequence_name.lower()
		
	return dataset_list


# Find the comp datasets: those which include other datasets along the axis Y
def find_comp_datasets(dataset_list, reviewed_dataset_list):
	for index, row in dataset_list.iterrows():
		sequence_id = row['sequence_id']
		if reviewed_dataset_list is not None:
			matching_index = reviewed_dataset_list[reviewed_dataset_list.sequence_id == sequence_id].first_valid_index()
			if matching_index:
				dataset_list.loc[index, 'included_datasets'] = reviewed_dataset_list.at[matching_index, 'included_datasets']
				continue
		patient_name = row['id_a']
		sequence_name = row['seq_nm_norm1']
		raw_acquisition_date = row['acquisition_date']
		origin = row['originY']
		end = row['endY']
		
		# find the siblings datasets with the same patient_name, acquisition_date and sequence_name (including the current dataset)
		siblings = dataset_list[(dataset_list['id_a'] == patient_name) & (dataset_list['acquisition_date'] == raw_acquisition_date) & (dataset_list['seq_nm_norm1'] == sequence_name)]

		# exclude the current dataset from the siblings
		siblings = siblings[siblings['sequence_id'] != sequence_id]

		# find duplicates (siblings which have the same originY and endY) and set the column duplicates_ids to the duplicate's sequence_ids
		duplicates = siblings[(siblings['originY'] == origin) & (siblings['endY'] == end)]
		dataset_list.at[index, 'duplicates_ids'] = str(list(duplicates['sequence_id']))

		# find the siblings which have an origin and end included in the current dataset
		# note that originY is greater than endY (in the current anima sagittal orientation)
		siblings = siblings[(siblings['originY'] <= origin) & (siblings['endY'] >= end)]
		# remove the duplicates from the siblings
		siblings = siblings[~siblings['sequence_id'].isin(duplicates['sequence_id'])]
		
		# if there are one or more such siblings: set the included_datasets colummn to the list of their sequence_id
		if len(siblings) > 0:
			dataset_list.at[index, 'included_datasets'] = str(list(siblings['sequence_id']))
	
	print("n datasets not comps:", len(dataset_list[dataset_list.included_datasets.isnull()]))
	return dataset_list

def sort_datasets_by_origin(dataset_list, reviewed_dataset_list=None):

	print('rank datasets by origin on axis Y...')

	# exclude comp datasets (those with no included_datasets) from the dataset_list, group the dataset_list by patient_name, acquisition_date and sequence_name and rank group items by originY (descending)
	dataset_list_without_comps = dataset_list[dataset_list.included_datasets.isnull()] if 'included_datasets' in dataset_list.columns else dataset_list
	dataset_list['originY_rank'] = dataset_list_without_comps.groupby(['id_a', 'acquisition_date', 'seq_nm_norm1'])['originY'].rank('dense', ascending=False)
	# set nan values to -1 in originY_rank
	dataset_list['originY_rank'].fillna(-1, inplace=True)
	# set the rank of the datasets with sequence_name containing 'COMP' to -1
	dataset_list.loc[dataset_list['sequence_name'].str.contains('comp', case=False), 'originY_rank'] = -1
	dataset_list.loc[dataset_list['sequence_name'].str.contains('rcdl', case=False), 'originY_rank'] = -1
	dataset_list.loc[dataset_list['sequence_name'].str.contains('mobiview', case=False), 'originY_rank'] = -1

	for index, row in dataset_list.iterrows():
		sequence_id = row['sequence_id']
		if reviewed_dataset_list is not None:
			matching_index = reviewed_dataset_list[reviewed_dataset_list.sequence_id == sequence_id].first_valid_index()
			if matching_index:
				dataset_list.loc[index, 'originY_rank'] = reviewed_dataset_list.at[matching_index, 'originY_rank']
				
	return dataset_list

def sort_axial_datasets(dataset_list, reviewed_dataset_list=None):
	# get axial datasets (those with AX in their sequence name)

	axial_datasets = dataset_list[dataset_list['axial'] == True]
	
	# for each axial dataset, find the SAG siblings datasets with the same patient_name, acquisition_date (excluding the current dataset) and SAG in their sequence_name
	for index, row in axial_datasets.iterrows():
		origin = row['originY']
		end = row['endY']
		sag_siblings = dataset_list[(dataset_list['id_a'] == row['id_a']) & (dataset_list['acquisition_date'] == row['acquisition_date']) & dataset_list['seq_nm_norm1'].str.contains('SAG')]
		
		# exclude datasets with originY_rank == -1
		sag_siblings = sag_siblings[sag_siblings['originY_rank'] != -1]
		# find the sag_siblings which include this datasets, knowing originY > endY (i.e. have a bigger origin and smaller end)
		sag_siblings = sag_siblings[(sag_siblings['originY'] >= origin) & (sag_siblings['endY'] <= end)]
		# if there are more than one sag_siblings
		if len(sag_siblings) > 0:
			# set the rank of this dataset to the most common rank of the sag_siblings
			dataset_list.at[index, 'originY_rank'] = sag_siblings['originY_rank'].mode()[0]
	
	# # group the axial datasets by patient_name, acquisition_date and sequence_name, rank them by originY and set the result to the column axial_rank (descending)
	dataset_list['axial_rank'] = axial_datasets.groupby(['id_a', 'acquisition_date', 'seq_nm_norm1'])['originY'].rank('dense', ascending=False)
	# add the axial rank column to the dataset list
	# dataset_list['axial_rank'] = axial_datasets['axial_rank']
	dataset_list['axial_rank'].fillna(0, inplace=True)

	for index, row in dataset_list.iterrows():
		sequence_id = row['sequence_id']
		if reviewed_dataset_list is not None:
			matching_index = reviewed_dataset_list[reviewed_dataset_list.sequence_id == sequence_id].first_valid_index()
			if matching_index:
				dataset_list.loc[index, 'axial_rank'] = reviewed_dataset_list.at[matching_index, 'axial_rank']
				dataset_list.loc[index, 'originY_rank'] = reviewed_dataset_list.at[matching_index, 'originY_rank']

	return dataset_list

def classify(learn_inf, reoriented_path, dataset_list):
	use_classifier = True
	test_files = [reoriented_path / image for image in dataset_list.image_preview_path]

	with tempfile.TemporaryDirectory() as test_path_name:
		test_path = Path(test_path_name)
		for image_path in test_files:

			im = Image.open(image_path)
			out = im.transpose(PIL.Image.FLIP_TOP_BOTTOM)

			out.save(test_path / f'{image_path.parent.name}_{image_path.name}')

		resize_images(test_path, max_size=400, dest=test_path)

		test_files = [test_path / image.replace('/', '_') for image in dataset_list.image_preview_path]

		test_dl = learn_inf.dls.test_dl(test_files)

		probs, _, cs = learn_inf.get_preds(dl=test_dl, with_decoded=True)

		dataset_list['predicted_label'] = learn_inf.dls.vocab[cs]
		dataset_list['predicted_probability'] = probs[range(cs.shape[0]), cs]
	
	# for index, row in dataset_list.iterrows():
	# 	sequence_id = row['sequence_id']
	# 	image_path = reoriented_path / row['image_preview_path']
	# 	prediction = learn_inf.predict(image_path)
	# 	dataset_list.loc[index, 'predicted_label'] = prediction[0]
	# 	dataset_list.loc[index, 'predicted_probability'] = prediction[2][prediction[1].item()].item()
	
	def decrement_if_brain_in_group(group):
		if 'brain' in list(group.predicted_label):
			group.loc[group['originY_rank'] > 0, 'originY_rank'] -= 1
		return group

	# if classified as brain: decrement all originY_rank of this exam (to check mismatches between prediction and originY_rank)
	dataset_list = dataset_list.groupby(['id_a', 'acquisition_date'], group_keys=False).apply(decrement_if_brain_in_group)

	classes = ['brain', 'cervical', 'thoracic', 'thoracic', 'thoracic'] # there can be multiple thoracic images ranked from 2 to 4

	# check mismatches between prediction and adjusted originY_rank, create a 'review' column for those mismatches
	for index, row in dataset_list.iterrows():
		sequence_id = row['sequence_id']
		predicted_label = row['predicted_label']
		originY_rank = row['originY_rank']
		axial = row['axial']

		if axial and (predicted_label != 'axial' or predicted_label != 'brain'):
			dataset_list.loc[index, 'review'] = f'prediction {predicted_label} mismatch with axial'
		elif not axial and classes[originY_rank] != predicted_label:
			dataset_list.loc[index, 'review'] = f'prediction {predicted_label} mismatch with originY rank {originY_rank} = {classes[originY_rank]}'

		if predicted_label in classes and use_classifier and dataset_list.at[index, 'originY_rank'] >= 0:
			dataset_list.loc[index, 'originY_rank'] = classes.index(predicted_label)
	
	thoracics = dataset_list[dataset_list['originY_rank'] == 2]
	dataset_list.loc[dataset_list['originY_rank'] == 2, 'originY_rank'] = thoracics.groupby(['id_a', 'acquisition_date', 'seq_nm_norm1'])['originY'].rank('dense', ascending=False) + 1

	return dataset_list

def sort_datasets(datasets_path, raw_path, reoriented_path, dataset_list, reviewed_dataset_list, data_sheet_path, learn_inf):
	print('reorient images')
	reorient_images(raw_path, reoriented_path)
	print('create images previews')
	create_image_previews(reoriented_path, dataset_list)

	print('filter datasets')
	dataset_list = filter_datasets(dataset_list)

	# save the dataset list to excel
	# dataset_list.to_excel(datasets_path / 'STIR-T2_SEQ_sorted.xlsx', index=False)

	# drop the column named Unnamed: 0
	# dataset_list.drop(columns=['Unnamed: 0'], inplace=True)


	# dataset_list = pandas.read_excel(datasets_path / 'STIR-T2_SEQ_sorted.xlsx')
	print('add origin end')
	dataset_list = add_origin_end(reoriented_path, dataset_list, reviewed_dataset_list)
	print('find comp datasets')
	dataset_list = find_comp_datasets(dataset_list, reviewed_dataset_list)
	print('sort_datasets_by_origin')
	dataset_list = sort_datasets_by_origin(dataset_list)

	dataset_list.loc[dataset_list.originY_rank == 'brain', 'originY_rank'] = '0'

	dataset_list = dataset_list.astype({'originY_rank':'int'})
	
	if reviewed_dataset_list != None:
		print('copy reviewed_dataset_list')
		for index, row in dataset_list.iterrows():
			sequence_id = row['sequence_id']
			if reviewed_dataset_list is not None:
				matching_index = reviewed_dataset_list[reviewed_dataset_list.sequence_id == sequence_id].first_valid_index()
				if matching_index:
					dataset_list.loc[index, 'axial_rank'] = reviewed_dataset_list.at[matching_index, 'axial_rank']
					dataset_list.loc[index, 'originY_rank'] = reviewed_dataset_list.at[matching_index, 'originY_rank']

	print('classify images')
	dataset_list = classify(learn_inf, reoriented_path, dataset_list)

	print('sort_axial_datasets')
	dataset_list = sort_axial_datasets(dataset_list)
	dataset_list = dataset_list.astype({'axial_rank':'int'})

	print('sort')
	# sort by patient_name, acquisition_date, originY_rank and sequence_name
	# dataset_list.sort_values(['id_a', 'acquisition_date', 'originY_rank', 'seq_nm_norm1'], inplace=True)
	dataset_list.sort_values(['id_a', 'acquisition_date', 'seq_nm_norm1', 'originY_rank'], inplace=True)

	# add a comment column (and set the first element to ' ' to prevent agGrid from ignoring this column)
	dataset_list['comment'] = ''
	dataset_list.iloc[0, list(dataset_list.columns).index('comment')] = ' '

	dataset_list.to_excel(data_sheet_path.parent / f'{data_sheet_path.stem}_sorted.xlsx', index=False)
	return

# --------------------------#
# --- Organize datasets --- #
# --------------------------#

def vectors_are_equal(vector1, vector2):
	return all( abs(v2-v1)<0.0001 for v1, v2 in zip(vector1, vector2))

def check_images_match(image1_path, image2_path, image1=None, image2=None, message_prefix=None):
	
	image1 = image1 or sitk.ReadImage(str(image1_path))
	image2 = image2 or sitk.ReadImage(str(image2_path))
	
	if message_prefix is None:
		message_prefix = str(image1_path) + ' and ' + str(image2_path)
	
	messages = []

	match = True
	if not vectors_are_equal(image1.GetSize(), image2.GetSize()):
		messages.append(f'{message_prefix} sizes do not match: {image1.GetSize()} - {image2.GetSize()}')
		match = False
	if not vectors_are_equal(image1.GetOrigin(), image2.GetOrigin()):
		messages.append(f'{message_prefix} origins do not match: {image1.GetOrigin()} - {image2.GetOrigin()}')
		match = False
	if not vectors_are_equal(image1.GetDirection(), image2.GetDirection()):
		messages.append(f'{message_prefix} directions do not match: {image1.GetDirection()} - {image2.GetDirection()}')
		match = False
	if not vectors_are_equal(image1.GetSpacing(), image2.GetSpacing()):
		messages.append(f'{message_prefix} spacings do not match: {image1.GetSpacing()} - {image2.GetSpacing()}')
		match = False

	for message in messages:
		print(message)

	return match, image1, image2

def images_are_duplicates(image1_path, image2_path):
	match, image1, image2 = check_images_match(image1_path, image2_path)
	if match and image1.GetSize() == image2.GetSize():
		data1 = sitk.GetArrayFromImage(image1)
		data2 = sitk.GetArrayFromImage(image2)
		return np.all(data1 == data2)
	return False

# organize the datasets in the following manner: patient_name / acquisition_date / originY_rank / f'{sequence_name}{axial_suffix}_{file_path.name}'
def organize_datasets_patient_date_part(datasets_path, patients_path, dataset_list, data_sheet_path):

	# delete patients_path if exists
	if patients_path.exists():
		shutil.rmtree(patients_path)

	duplicates = []
	unknowns = []
	for index, row in dataset_list.iterrows():
		sequence_name = row['seq_nm_norm1']
		# replace spaces by _ in sequence_name
		sequence_name = re.sub('[^a-zA-Z0-9\.]', '_', sequence_name)
		raw_acquisition_date = row['acquisition_date']
		if type(raw_acquisition_date) == str:
			# raw_acquisition_date = datetime.datetime.strptime(raw_acquisition_date, "%m/%d/%Y")
			raw_acquisition_date = datetime.datetime.strptime(raw_acquisition_date[:10], "%Y-%m-%d")
		acquisition_date = raw_acquisition_date.strftime('%Y-%m-%d')
		patient_name = str(row['id_a'])
		print(patient_name, acquisition_date, sequence_name)
		# find the corresponding file
		file_path = row['image_path']
		if pandas.isna(file_path): continue
		# replace '/data/amasson/medullar/datasets2/reoriented' with '/data/amasson/medullar/datasets2/raw'
		file_path = datasets_path / 'raw' / file_path
		label = str(row['originY_rank']) # if row['originY_rank'] != '-1' else 'comp'
		# create the destination folder
		destination_folder = patients_path / patient_name / acquisition_date / label
		# create the destination folder if it does not exist
		destination_folder.mkdir(parents=True, exist_ok=True)
		# create the axial suffix f'_{axial_rank}' if the dataset is axial
		# axial_suffix = f'_ax{int(row["axial_rank"])}' if 'AX' in sequence_name else '_ax' if row['axial'] else ''
		# rename according to https://gitlab.inria.fr/primuswp31/weeklyseminar/-/blob/main/22_04_28/scseg_db.md
		orientation = 'Ax' if row['axial'] else 'Sag'
		file_name = re.sub('[^a-zA-Z0-9\.]', '_', file_path.name) 	# file_path.name.replace(' ', '_').replace('§', '_')
		
		modality = None
		for name in [sequence_name, file_name]:
			name = name.lower()
			modality = 't2s' if 't2' in name and 'star' in name else 't2' if 't2' in name else 'stir' if 'stir' in name else 'ofsepDualTse' if 'ofsep_dual_tse' in name else None
			if modality is not None: break
		
		if modality is None:
			unknowns.append(index)
			print('Unknown modality for', patient_name, acquisition_date, file_name, file_path)
			modality = ''
		
		link = Path(destination_folder / f'{modality}{orientation}__{file_name}')
	
		# set file_name to file name without special characters and with _ instead of spaces
		# create the symlink
		# link = Path(destination_folder / f'{sequence_name}{axial_suffix}_{file_name}')
		# while the symlink exists, increment its id and use the following pattern f'{sequence_name}{axial_suffix}_{id}_{file_name}')
		id = 1
		while link is not None and link.exists():
			link2 = Path(destination_folder / f'{modality}{orientation}_{id}__{file_name}')
			if images_are_duplicates(link.resolve(), file_path):
				print('Warning: found exacte duplicates', link2)
				duplicates.append(index)
				link = None
			else:
				print('Warning: found duplicates', link2)
				link = link2
			id += 1
		if link is not None:
			link.symlink_to(file_path)
	
	if len(duplicates)>0:
		print('Duplicates:')
		duplicates = dataset_list.loc[duplicates]
		print(duplicates)
		duplicates.to_csv(data_sheet_path.parent / f'{data_sheet_path.stem}_duplicates.csv', index=False)
	
	if len(unknowns)>0:
		print('Unknown modalities:')
		unknowns = dataset_list.loc[unknowns]
		print(unknowns)
		unknowns.to_csv(data_sheet_path.parent / f'{data_sheet_path.stem}_unknowns.csv', index=False)
	
	return dataset_list

def organize_datasets(datasets_path, patients_path, dataset_list, data_sheet_path):
	# dataset_list = pandas.read_excel(data_sheet_path.parent / f'{data_sheet_path.stem}_sorted.xlsx')
	organize_datasets_patient_date_part(datasets_path, patients_path, dataset_list, data_sheet_path)

parser = argparse.ArgumentParser(
    prog=__file__,
    formatter_class=argparse.RawTextHelpFormatter,
    description=textwrap.dedent("""Organize datasets
	
	See Readme.md for full usage information.

	Organizing datasets involves:
	- reorienting images (since we want to sort them by origin.y) ; images will be saved in the reoriented folder in the datasets_path
	- creating previews
	- filtering unwanted images like DTI, GADO and comp images
	- sorting images by origin on the Y axis
	- save the sorted images data in an excel file
	- manually edit this excel file (relabel images) with the dedicated web interface. Comp should be labeled as -1, brain as 0, cerv as 1, thor as 2, etc.
	- use the excel file to create a structured folder: patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{file_path.name}'

	This happens in three steps:
	  1. Sort datasets with the -sort option: it will create an excel file (data_sheet_sorted.xlsx) with the sorted datasets. 
	  2. Edit this excel file to relabel images with the dedicated web interface.
	  3. Organize datasets with the -organize option. This will create a structured folder: patient_name / acquisition_date / part_number / f'{sequence_name}{axial_suffix}_{file_path.name}'

"""))

parser.add_argument('-d', '--datasets_path', type=str, required=True, help='the path to the datasets folder')
parser.add_argument('-ds', '--data_sheet', type=str, default='/data/amasson/medullar/datasets2/STIR-T2_SEQ.xlsx', help='the data sheet (excel format). Default is /data/amasson/medullar/datasets2/STIR-T2_SEQ.xlsx')
parser.add_argument('-rds', '--reviewed_data_sheet', type=str, default=None, help='an optional datasheet already reviewed with the viewer. The originY_rank will be copied from this reviewed datasheet to the --data_sheet.')
parser.add_argument('-s', '--sort', action='store_true', help='sort the images by origin on the Y axis')
parser.add_argument('-o', '--organize', action='store_true', help='organize the datasets')
args = parser.parse_args()

learn_inf = load_learner('models/classifier.pkl')

# Initialize paths
datasets_path = Path(args.datasets_path) # Path('/data/amasson/medullar/datasets2/')
if datasets_path.name == 'raw':
	print('Warning: datasets path name is raw, using parent folder instead: ', datasets_path.parent)
	datasets_path = datasets_path.parent

raw_path = datasets_path / 'raw'
patients_path = datasets_path / 'structured'
patients_path.mkdir(parents=True, exist_ok=True)
reoriented_path = datasets_path / 'reoriented'
reoriented_path.mkdir(parents=True, exist_ok=True)

# Read datasets list
data_sheet_path = Path(args.data_sheet)
dataset_list = pandas.read_excel(data_sheet_path) if data_sheet_path.name.endswith('.xls') or data_sheet_path.name.endswith('.xlsx') else pandas.read_csv(data_sheet_path)

reviewed_dataset_list = None
if args.reviewed_data_sheet:
	reviewed_data_sheet_path = Path(args.reviewed_data_sheet)
	reviewed_dataset_list = pandas.read_excel(reviewed_data_sheet_path) if reviewed_data_sheet_path.name.endswith('.xls') or reviewed_data_sheet_path.name.endswith('.xlsx') else pandas.read_csv(reviewed_data_sheet_path)

dtype = dataset_list.dtypes


if args.sort:
	sort_datasets(datasets_path, raw_path, reoriented_path, dataset_list, reviewed_dataset_list, data_sheet_path, learn_inf)
if args.organize:
	organize_datasets(datasets_path, patients_path, reviewed_dataset_list if args.reviewed_data_sheet else dataset_list, data_sheet_path)
